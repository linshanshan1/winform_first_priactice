﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_MouseClick(object sender, MouseEventArgs e)
        {
            BackColor = Color.Blue;
        }

        private void Form2_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            BackColor = Color.Red;
        }

        private void Form2_Click(object sender, EventArgs e)
        {
            var a = MessageBox.Show("选择同意", "消息标题", MessageBoxButtons.YesNoCancel,MessageBoxIcon.Information);
            if (a == DialogResult.Yes)
            {
                MessageBox.Show("同意了");
            }
            else
            {
                MessageBox.Show("不同意");
            }
        }
    }
}
